<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
	wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
    
 
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/*
    Gravity Forms => 1.9
*/

add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}

function year_shortcode() {
  $year = date('Y');
  return $year;
}
add_shortcode('year', 'year_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
  if ( isset( $atts['facet'] ) ) {       
      $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
  }
  return $output;
}, 10, 2 );




function wphe_body_classes( $classes ) {
    global $post;
     
    if ( is_singular( 'post' ) ) {  
        $classes[] = 'single';  // add custom class to all single posts
    }
     
    if ( is_singular( 'page' ) ) {  
        $classes[] = 'page';  // add custom class to all single pages
    }   
       
    if ( is_home() ) {  
        $classes[] = 'home';  // add custom class to blog homepage
    }           
                    
 
    return $classes;
}
add_filter( 'body_class', 'wphe_body_classes' );
